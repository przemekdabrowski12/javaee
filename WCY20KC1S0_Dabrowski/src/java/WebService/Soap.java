/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebService;

import EJB.DBConnect;
import JPA.WCY20KC1S0Dabrowski;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.zip.CRC32;

@WebService(serviceName = "Soap")
public class Soap {

    DBConnect obj = new DBConnect();
    Connection con = obj.connect();
    
    @WebMethod(operationName = "generate")
    public String generate(@WebParam(name = "message") String message) throws NoSuchAlgorithmException{
        CRC32 crc32 = new CRC32();
        crc32.update(message.getBytes());
        long hashValue = crc32.getValue();
        return Long.toHexString(hashValue);
    }
    
    @WebMethod(operationName = "display")
    public String display(){           
        return WCY20KC1S0Dabrowski.getListaHTML();
    }
    
    @WebMethod(operationName = "addHash")
    public void addHash(@WebParam(name = "creator") String creator, @WebParam(name = "hash") String hash) throws SQLException, ClassNotFoundException{
         WCY20KC1S0Dabrowski.addHash(creator, hash);
    }
    
    @WebMethod(operationName = "DeleteHash")
    public void DeleteHash(@WebParam(name = "id") String id){
        WCY20KC1S0Dabrowski.deleteHash(id);
    }
    
}

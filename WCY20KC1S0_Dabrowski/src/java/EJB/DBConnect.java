package EJB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class DBConnect {

    private final String DRIVER = "org.mariadb.jdbc.Driver";
    private final String JDBC_URL = "jdbc:mariadb://10.103.1.26:3306/studenci_2020";
    public java.sql.Connection conn;

    public Connection connect() {
        try{
            Class.forName(DRIVER);
            conn = DriverManager.getConnection(JDBC_URL, "student", "student-TJEE");
        }catch(ClassNotFoundException | SQLException e){
            Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null, e);
        }
        return conn;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.TypedQuery;

/**
 *
 * @creator student
 */
@Entity
@Table(name = "WCY20KC1S0_Dabrowski")
@NamedQueries({
    @NamedQuery(name = "WCY20KC1S0Dabrowski.findAll", query = "SELECT w FROM WCY20KC1S0Dabrowski w"),
    @NamedQuery(name = "WCY20KC1S0Dabrowski.findById", query = "SELECT w FROM WCY20KC1S0Dabrowski w WHERE w.id = :id"),
    @NamedQuery(name = "WCY20KC1S0Dabrowski.findByCreator", query = "SELECT w FROM WCY20KC1S0Dabrowski w WHERE w.creator = :creator"),
    @NamedQuery(name = "WCY20KC1S0Dabrowski.findByHash", query = "SELECT w FROM WCY20KC1S0Dabrowski w WHERE w.hash = :hash")})
public class WCY20KC1S0Dabrowski implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "creator")
    private String creator;
    @Basic(optional = false)
    @Column(name = "hash")
    private String hash;

    public WCY20KC1S0Dabrowski() {
    }

    public WCY20KC1S0Dabrowski(Integer id) {
        this.id = id;
    }

    public WCY20KC1S0Dabrowski(Integer id, String creator, String hash) {
        this.id = id;
        this.creator = creator;
        this.hash = hash;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WCY20KC1S0Dabrowski)) {
            return false;
        }
        WCY20KC1S0Dabrowski other = (WCY20KC1S0Dabrowski) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JPA.WCY20KC1S0Dabrowski[ id=" + id + " ]";
    }
    
    public static String getListaHTML() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WCY20KC1S0_DabrowskiPU");
        EntityManager em = emf.createEntityManager();
        TypedQuery<WCY20KC1S0Dabrowski> query = em.createNamedQuery("WCY20KC1S0Dabrowski.findAll", WCY20KC1S0Dabrowski.class);
        List<WCY20KC1S0Dabrowski> lista = query.getResultList();
        
        String wiersz;  
        String temp,temp2;
        
        wiersz = ("<center><table><tr>");  
        
        wiersz = wiersz.concat(
                "<td><b>ID</b></td>" 
                + "<td><b>CREATOR</b></td>" 
                + "<td><b>HASH</b></td>"
                + "<td><b>ACTION</b></td>");
        
        wiersz = wiersz.concat("</tr>");
        
        for (WCY20KC1S0Dabrowski hash : lista) {            
            wiersz = wiersz.concat("<tr>");
            wiersz = wiersz.concat("<td>" + hash.getId() + "</td>");
            wiersz = wiersz.concat("<td>" + hash.getCreator() + "</td>");            
            wiersz = wiersz.concat("<td>" + hash.getHash() + "</td>");  
            temp = "delete?hashid=" + hash.getId();
            wiersz = wiersz.concat("<td><a href="+temp+"><input type=\"button\" value=\"Delete\" /></a></td>");
            wiersz = wiersz.concat("</tr>");        
        }        
        
        wiersz = wiersz.concat("</table></center>");
        
        return wiersz;    
    
    }
    
    public static void addHash(String creator, String hash) throws SQLException, ClassNotFoundException{
    
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WCY20KC1S0_DabrowskiPU");
        EntityManager em = emf.createEntityManager();   
        em.getTransaction().begin();
        
        TypedQuery<WCY20KC1S0Dabrowski> query = em.createNamedQuery("WCY20KC1S0Dabrowski.findAll", WCY20KC1S0Dabrowski.class);
        List<WCY20KC1S0Dabrowski> lista = query.getResultList();
             
        int maxid = 0;
        for(WCY20KC1S0Dabrowski temp : lista){
            maxid = temp.getId();
        }
                      
        WCY20KC1S0Dabrowski newhash = new WCY20KC1S0Dabrowski();    
                   
        newhash.setId(maxid + 1);
        newhash.setCreator(creator);
        newhash.setHash(hash);
        
        em.persist(newhash);
        em.flush();
        
        em.getTransaction().commit();            
       
    }
         
    public static void deleteHash(String id){
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WCY20KC1S0_DabrowskiPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        Query query = em.createQuery("DELETE FROM WCY20KC1S0Dabrowski w where w.id = :id");
        query.setParameter("id", Integer.valueOf(id)).executeUpdate();

    }
}

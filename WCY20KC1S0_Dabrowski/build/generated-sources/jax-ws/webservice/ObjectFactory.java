
package webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ClassNotFoundException_QNAME = new QName("http://WebService/", "ClassNotFoundException");
    private final static QName _DeleteHash_QNAME = new QName("http://WebService/", "DeleteHash");
    private final static QName _DeleteHashResponse_QNAME = new QName("http://WebService/", "DeleteHashResponse");
    private final static QName _NoSuchAlgorithmException_QNAME = new QName("http://WebService/", "NoSuchAlgorithmException");
    private final static QName _SQLException_QNAME = new QName("http://WebService/", "SQLException");
    private final static QName _AddHash_QNAME = new QName("http://WebService/", "addHash");
    private final static QName _AddHashResponse_QNAME = new QName("http://WebService/", "addHashResponse");
    private final static QName _Display_QNAME = new QName("http://WebService/", "display");
    private final static QName _DisplayResponse_QNAME = new QName("http://WebService/", "displayResponse");
    private final static QName _Generate_QNAME = new QName("http://WebService/", "generate");
    private final static QName _GenerateResponse_QNAME = new QName("http://WebService/", "generateResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ClassNotFoundException }
     * 
     */
    public ClassNotFoundException createClassNotFoundException() {
        return new ClassNotFoundException();
    }

    /**
     * Create an instance of {@link DeleteHash }
     * 
     */
    public DeleteHash createDeleteHash() {
        return new DeleteHash();
    }

    /**
     * Create an instance of {@link DeleteHashResponse }
     * 
     */
    public DeleteHashResponse createDeleteHashResponse() {
        return new DeleteHashResponse();
    }

    /**
     * Create an instance of {@link NoSuchAlgorithmException }
     * 
     */
    public NoSuchAlgorithmException createNoSuchAlgorithmException() {
        return new NoSuchAlgorithmException();
    }

    /**
     * Create an instance of {@link SQLException }
     * 
     */
    public SQLException createSQLException() {
        return new SQLException();
    }

    /**
     * Create an instance of {@link AddHash }
     * 
     */
    public AddHash createAddHash() {
        return new AddHash();
    }

    /**
     * Create an instance of {@link AddHashResponse }
     * 
     */
    public AddHashResponse createAddHashResponse() {
        return new AddHashResponse();
    }

    /**
     * Create an instance of {@link Display }
     * 
     */
    public Display createDisplay() {
        return new Display();
    }

    /**
     * Create an instance of {@link DisplayResponse }
     * 
     */
    public DisplayResponse createDisplayResponse() {
        return new DisplayResponse();
    }

    /**
     * Create an instance of {@link Generate }
     * 
     */
    public Generate createGenerate() {
        return new Generate();
    }

    /**
     * Create an instance of {@link GenerateResponse }
     * 
     */
    public GenerateResponse createGenerateResponse() {
        return new GenerateResponse();
    }

    /**
     * Create an instance of {@link SqlException }
     * 
     */
    public SqlException createSqlException() {
        return new SqlException();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link Throwable }
     * 
     */
    public Throwable createThrowable() {
        return new Throwable();
    }

    /**
     * Create an instance of {@link StackTraceElement }
     * 
     */
    public StackTraceElement createStackTraceElement() {
        return new StackTraceElement();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClassNotFoundException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClassNotFoundException }{@code >}
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "ClassNotFoundException")
    public JAXBElement<ClassNotFoundException> createClassNotFoundException(ClassNotFoundException value) {
        return new JAXBElement<ClassNotFoundException>(_ClassNotFoundException_QNAME, ClassNotFoundException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteHash }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteHash }{@code >}
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "DeleteHash")
    public JAXBElement<DeleteHash> createDeleteHash(DeleteHash value) {
        return new JAXBElement<DeleteHash>(_DeleteHash_QNAME, DeleteHash.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteHashResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteHashResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "DeleteHashResponse")
    public JAXBElement<DeleteHashResponse> createDeleteHashResponse(DeleteHashResponse value) {
        return new JAXBElement<DeleteHashResponse>(_DeleteHashResponse_QNAME, DeleteHashResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoSuchAlgorithmException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link NoSuchAlgorithmException }{@code >}
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "NoSuchAlgorithmException")
    public JAXBElement<NoSuchAlgorithmException> createNoSuchAlgorithmException(NoSuchAlgorithmException value) {
        return new JAXBElement<NoSuchAlgorithmException>(_NoSuchAlgorithmException_QNAME, NoSuchAlgorithmException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SQLException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SQLException }{@code >}
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "SQLException")
    public JAXBElement<SQLException> createSQLException(SQLException value) {
        return new JAXBElement<SQLException>(_SQLException_QNAME, SQLException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddHash }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddHash }{@code >}
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "addHash")
    public JAXBElement<AddHash> createAddHash(AddHash value) {
        return new JAXBElement<AddHash>(_AddHash_QNAME, AddHash.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddHashResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddHashResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "addHashResponse")
    public JAXBElement<AddHashResponse> createAddHashResponse(AddHashResponse value) {
        return new JAXBElement<AddHashResponse>(_AddHashResponse_QNAME, AddHashResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Display }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Display }{@code >}
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "display")
    public JAXBElement<Display> createDisplay(Display value) {
        return new JAXBElement<Display>(_Display_QNAME, Display.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisplayResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DisplayResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "displayResponse")
    public JAXBElement<DisplayResponse> createDisplayResponse(DisplayResponse value) {
        return new JAXBElement<DisplayResponse>(_DisplayResponse_QNAME, DisplayResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Generate }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Generate }{@code >}
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "generate")
    public JAXBElement<Generate> createGenerate(Generate value) {
        return new JAXBElement<Generate>(_Generate_QNAME, Generate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GenerateResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "generateResponse")
    public JAXBElement<GenerateResponse> createGenerateResponse(GenerateResponse value) {
        return new JAXBElement<GenerateResponse>(_GenerateResponse_QNAME, GenerateResponse.class, null, value);
    }

}

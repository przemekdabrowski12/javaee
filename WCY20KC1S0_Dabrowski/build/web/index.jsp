<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>Main page</title>
        </head>
        <body>
            <div style="text-align: center">
                <h1><h:outputText value="MENU"/></h1>
                <a href="display" ><input type="button" value="View hashes from database"/></a>
                <a href="generate.jsp" ><input type="button" value="Calculate new hash"/></a>
            </div>
        </body>
    </html>
</f:view>

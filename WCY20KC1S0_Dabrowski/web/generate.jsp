<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Generate</title>
    </head>
    <body>
        <div style="text-align: center">
            <h1 >Enter following data: </h1>
                <form action="generate" method="POST">
                    <label for="creator">Creator </label>
                    <input type="text" id="creator" name="creator" /><br />
                    <label for="hash">Message to hash: </label>
                    <input type="text" id="hash" name="hash" /><br />           
                    <input  type="submit" value="Generate hash!" />
                    <a href="index.jsp"><input  type="button" value="Return to main page"/></a>
                </form>
        </div>
    </body>
</html>
